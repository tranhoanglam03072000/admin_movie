import axios from "axios";
import { userInfoLocal } from "./localService";
export const TOKEN_TRANHOANGLAM =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzM0UiLCJIZXRIYW5TdHJpbmciOiIyNi8wNC8yMDIzIiwiSGV0SGFuVGltZSI6IjE2ODI0NjcyMDAwMDAiLCJuYmYiOjE2NTQzNjIwMDAsImV4cCI6MTY4MjYxNDgwMH0.GGqFf8-ZXIqAjnaJZ40LjQvUHb1VyvRv3XtEIsMe_qE";
export const BASE_URL = "https://movienew.cybersoft.edu.vn";
export const configHeader = () => {
  return {
    TokenCybersoft: TOKEN_TRANHOANGLAM,
  };
};
export let MA_NHOM = "GP01";
export let https = axios.create({
  baseURL: BASE_URL,
  headers: {
    TokenCybersoft: TOKEN_TRANHOANGLAM,
    Authorization: "Bearer " + userInfoLocal.get()?.accessToken,
  },
});
