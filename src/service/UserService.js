import { https } from "./urlConfig";

export const userServie = {
  postLogin: (user) => {
    let uri = "/api/QuanLyNguoiDung/DangNhap";
    return https.post(uri, user);
  },
  getListUsers: (params) => {
    let uri = `/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${params.MaNhom}`;
    return https.get(uri);
  },
  deleteUser: (TaiKhoan) => {
    let uri = `/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${TaiKhoan}`;
    return https.delete(uri);
  },
  addUser: (dataUser) => {
    let uri = `/api/QuanLyNguoiDung/ThemNguoiDung`;
    return https.post(uri, dataUser);
  },
  updateUser: (dataUser) => {
    let uri = `/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`;
    return https.post(uri, dataUser);
  },
};
