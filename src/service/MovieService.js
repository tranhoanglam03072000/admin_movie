import { https } from "./urlConfig";

export let MovieService = {
  getListMovie: (params) => {
    let uri = `/api/QuanLyPhim/LayDanhSachPhim`;
    return https.get(uri, { params });
  },
  deleteMovie: (maPhim) => {
    let uri = `/api/QuanLyPhim/XoaPhim?MaPhim=${maPhim}`;
    return https.delete(uri);
  },
  addMovie: (formData) => {
    let uri = `/api/QuanLyPhim/ThemPhimUploadHinh`;
    return https.post(uri, formData);
  },
};
