import SecureView from "../HOC/SecureView";
import HomePage from "../page/HomePage";
import LoginPage from "../page/LoginPage";

export let arrRoute = [
  {
    path: "/",
    element: <SecureView Component={HomePage} />,
  },
  {
    path: "/login",
    element: <LoginPage />,
  },
];
