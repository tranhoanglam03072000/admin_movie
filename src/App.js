import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { arrRoute } from "./Routers/routers";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {arrRoute.map((route, index) => {
            return (
              <Route key={index} path={route.path} element={route.element} />
            );
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
