import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setUserLogOut } from "../../Redux/userSlice";
import { userInfoLocal } from "../../service/localService";

export default function HeaderPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  let handleLogOut = () => {
    userInfoLocal.remove();
    dispatch(setUserLogOut());
    navigate("/login");
  };
  return (
    <div className="px-3">
      <span className="text-white mr-6">Name</span>
      <button
        onClick={handleLogOut}
        className="bg-white rounded px-2 py-2 hover:bg-gray-200  duration-200"
      >
        Đăng xuất
      </button>
    </div>
  );
}
