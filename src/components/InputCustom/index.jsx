import React from "react";
import style from "./styles/Input.module.css";
export default function InputCustom({
  name,
  value,
  label,
  valueErr,
  handleChangeValue,
  refInputDisble,
}) {
  return (
    <div>
      <main className={`${style.field} ${style.field_v1}`}>
        <label className={`${style.ha_screen_reader} ${style}`}>{label}</label>
        <input
          ref={refInputDisble}
          onChange={handleChangeValue}
          className={`${style.field__input}`}
          placeholder="*"
          name={name}
          value={value}
        />
        <span className={`${style.field__label_wrap}`} aria-hidden="true">
          <span className={`${style.field__label}`}>{label}</span>
        </span>
      </main>
      <div className="h-6 text-red-500"> {valueErr}</div>
    </div>
  );
}
