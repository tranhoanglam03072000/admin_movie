import { MovieService } from "../service/MovieService";
import { MA_NHOM } from "../service/urlConfig";

const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  listMovies: [],
};

export const adminMovieSlice = createSlice({
  name: "adminMovieSlice",
  initialState,
  reducers: {
    setListMovies: (state, { payload }) => {
      let listMovie = payload.map((movie) => {
        return { ...movie, key: movie.maPhim };
      });
      state.listMovies = listMovie;
    },
  },
});

export const getListMovieAsync = () => {
  return (dispatch) => {
    let fetchApi = async () => {
      let params = {
        MaNhom: MA_NHOM,
      };
      try {
        let resMovie = await MovieService.getListMovie(params);
        dispatch(setListMovies(resMovie.data.content));
      } catch (err) {
        console.log(err);
      }
    };
    fetchApi();
  };
};
export const { setListMovies } = adminMovieSlice.actions;
export default adminMovieSlice.reducer;
