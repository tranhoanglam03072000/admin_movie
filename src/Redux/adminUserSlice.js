import { MA_NHOM } from "../service/urlConfig";
import { userServie } from "../service/UserService";

const { createSlice, current } = require("@reduxjs/toolkit");
const initialState = {
  listUsers: [],
  userEdit: {},
  statusShowEditUser: false,
};
const adminUserSlice = createSlice({
  name: "adminUserSlice",
  initialState,
  reducers: {
    setListUser: (state, { payload }) => {
      let listUser = payload.map((user) => {
        return { ...user, key: user.taiKhoan };
      });
      state.listUsers = listUser;
    },
    setUserEditAction: (state, { payload }) => {
      let newUserEdit = current(state.listUsers).find(
        (user) => user.taiKhoan === payload
      );
      state.userEdit = newUserEdit;
    },
    setStatusShowEditUser: (state, { payload }) => {
      state.statusShowEditUser = payload;
    },
  },
});

export const getListUserAsync = () => {
  return (dispatch) => {
    let fetchApi = async () => {
      let params = {
        MaNhom: MA_NHOM,
      };
      try {
        let resUser = await userServie.getListUsers(params);
        dispatch(setListUser(resUser.data.content));
      } catch (err) {
        console.log(err);
      }
    };
    fetchApi();
  };
};
export const { setListUser, setUserEditAction, setStatusShowEditUser } =
  adminUserSlice.actions;
export default adminUserSlice.reducer;
