import { userInfoLocal } from "../service/localService";
import { userServie } from "../service/UserService";

const { createSlice } = require("@reduxjs/toolkit");
const initialState = {
  isLogin: false,
  userInfo: userInfoLocal.get(),
};
const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserInfo: (state, { payload }) => {
      let { userInfo } = payload;
      state.userInfo = userInfo;
      state.isLogin = true;
    },
    setUserLogOut: (state, { payload }) => {
      state.userInfo = null;
    },
  },
});

export const postLoginAsync = ({ user, handleNavige }) => {
  return (dispatch) => {
    let postLogin = async ({ user }) => {
      try {
        let res = await userServie.postLogin(user);
        let userInfo = res.data.content;
        dispatch(setUserInfo({ userInfo }));
        userInfoLocal.set(userInfo);
        handleNavige();
      } catch (err) {
        console.log(err);
      }
    };
    postLogin({ user });
  };
};
export const { setUserInfo, setUserLogOut } = userSlice.actions;
export default userSlice.reducer;
