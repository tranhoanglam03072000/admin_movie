import React, { useState } from "react";
import { message, Modal } from "antd";
import InputCustom from "../../../components/InputCustom";
import { MA_NHOM } from "../../../service/urlConfig";
import {
  changeValueValidation,
  submitFormValidation,
} from "./utils/validationUser";
import { useDispatch } from "react-redux";
import { submitCreateFormApi } from "./apis/apiUser";
export default function AddUser() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const dispatch = useDispatch();
  let [valueInput, setValue] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDt: "",
    maNhom: MA_NHOM,
    maLoaiNguoiDung: "",
    hoTen: "",
  });
  let [valueErr, setValueErr] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDt: "",
    maLoaiNguoiDung: "",
    hoTen: "",
  });
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const handleChangeValue = (e) => {
    let { name, value } = e.target;
    changeValueValidation(value, name, valueErr, setValueErr);
    setValue({ ...valueInput, [name]: value });
  };
  const handleSubmitForm = (e) => {
    e.preventDefault();
    let isValid = submitFormValidation(valueInput, valueErr);
    if (isValid) {
      submitCreateFormApi(
        handleCancel,
        setValue,
        setValueErr,
        valueErr,
        valueInput,
        dispatch
      );
    } else {
      message.error("đăng kí thất bại");
    }
  };
  const handleResetForm = (e) => {
    e.preventDefault();
    setValue({
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      maNhom: MA_NHOM,
      maLoaiNguoiDung: "",
      hoTen: "",
    });
    setValueErr({
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      maLoaiNguoiDung: "",
      hoTen: "",
    });
  };
  return (
    <>
      <button
        className="px-2 py-2 bg-purple-500 rounded-lg text-white hover:bg-purple-700 duration-200"
        onClick={showModal}
      >
        Thêm người dùng
      </button>
      <Modal
        title="Thêm người dùng"
        open={isModalOpen}
        onCancel={handleCancel}
        width={600}
        footer={[
          <button
            key="reset"
            onClick={handleResetForm}
            className="bg-purple-500 hover:bg-purle-600 hover:text-white duration-200 text-white px-3 py-2 rounded"
          >
            Làm mới
          </button>,
          <button
            key="submit"
            onClick={handleSubmitForm}
            className="bg-red-500 hover:bg-red-600 hover:text-white duration-200 text-white px-3 py-2 rounded ml-4"
          >
            Xác nhận
          </button>,
        ]}
      >
        <form className="grid grid-cols-2 gap-10">
          <InputCustom
            handleChangeValue={handleChangeValue}
            label={"Tài Khoản"}
            name={"taiKhoan"}
            value={valueInput.taiKhoan}
            valueErr={valueErr.taiKhoan}
          />
          <InputCustom
            handleChangeValue={handleChangeValue}
            label={"Mật Khẩu"}
            name={"matKhau"}
            value={valueInput.matKhau}
            valueErr={valueErr.matKhau}
          />
          <InputCustom
            handleChangeValue={handleChangeValue}
            label={"Số điên thoại"}
            name={"soDt"}
            value={valueInput.soDt}
            valueErr={valueErr.soDt}
          />
          <InputCustom
            handleChangeValue={handleChangeValue}
            label={"email"}
            name={"email"}
            value={valueInput.email}
            valueErr={valueErr.email}
          />
          <InputCustom
            handleChangeValue={handleChangeValue}
            label={"Họ tên"}
            name={"hoTen"}
            value={valueInput.hoTen}
            valueErr={valueErr.hoTen}
          />
          {/* Select type customer */}
          <div className="flex items-center mt-7">
            <div className="w-full h-full">
              <select
                className="w-full h-2/3 border shadow-md rounded"
                name="maLoaiNguoiDung"
                onChange={handleChangeValue}
              >
                <option value="">Mã loại khách hàng</option>
                <option value="khachHang">Khách hàng</option>
                <option value="quanTri">Quản trị</option>
              </select>
              <div className="h-6 text-red-500">{valueErr.maLoaiNguoiDung}</div>
            </div>
          </div>
        </form>
      </Modal>
    </>
  );
}
