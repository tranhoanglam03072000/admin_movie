import { message, Modal } from "antd";
import React, { Fragment, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import InputCustom from "../../../components/InputCustom";
import { setStatusShowEditUser } from "../../../Redux/adminUserSlice";
import { MA_NHOM } from "../../../service/urlConfig";
import { submitUpdateFormApi } from "./apis/apiUser";
import {
  changeValueValidation,
  submitFormValidation,
} from "./utils/validationUser";

export default function EditUser({}) {
  let refInputDisble = useRef();
  let [valueInput, setValue] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDt: "",
    maNhom: MA_NHOM,
    maLoaiNguoiDung: "",
    hoTen: "",
  });
  let [valueErr, setValueErr] = useState({
    taiKhoan: "",
    matKhau: "",
    email: "",
    soDt: "",
    maLoaiNguoiDung: "",
    hoTen: "",
  });
  let dispatch = useDispatch();
  let { userEdit, statusShowEditUser } = useSelector(
    (state) => state.adminUserSlice
  );
  useEffect(() => {
    refInputDisble.current?.setAttribute("disabled", true);
    const { taiKhoan, matKhau, email, soDT, maLoaiNguoiDung, hoTen } = userEdit;
    setValue({
      taiKhoan: taiKhoan || "",
      matKhau: matKhau || "",
      email: email || "",
      soDt: soDT || "",
      maNhom: MA_NHOM || "",
      maLoaiNguoiDung: maLoaiNguoiDung || "",
      hoTen: hoTen || "",
    });
  }, [userEdit.taiKhoan]);
  const handleChangeValue = (e) => {
    let { name, value } = e.target;
    changeValueValidation(value, name, valueErr, setValueErr);
    setValue({ ...valueInput, [name]: value });
  };
  const handleSubmitForm = (e) => {
    e.preventDefault();
    let isValid = submitFormValidation(valueInput, valueErr);
    if (isValid) {
      submitUpdateFormApi(valueInput, dispatch);
    } else {
      message.error("đăng kí thất bại");
    }
  };
  const handleResetForm = (e) => {
    e.preventDefault();
    setValue({
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      maNhom: MA_NHOM,
      maLoaiNguoiDung: "",
      hoTen: "",
    });
    setValueErr({
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      maLoaiNguoiDung: "",
      hoTen: "",
    });
  };
  return (
    <Fragment>
      <Modal
        title="Sửa thông tin người dùng"
        centered
        open={statusShowEditUser}
        onOk={() => dispatch(setStatusShowEditUser(false))}
        onCancel={() => dispatch(setStatusShowEditUser(false))}
        width={600}
        footer={[
          <button
            key="resetForm"
            onClick={handleResetForm}
            className="bg-purple-500 hover:bg-purle-600 hover:text-white duration-200 text-white px-3 py-2 rounded"
          >
            Làm mới
          </button>,
          <button
            key="submitForm"
            onClick={handleSubmitForm}
            className="bg-red-500 hover:bg-red-600 hover:text-white duration-200 text-white px-3 py-2 rounded ml-4"
          >
            Xác nhận
          </button>,
        ]}
      >
        <form className="grid grid-cols-2 gap-10">
          <InputCustom
            refInputDisble={refInputDisble}
            handleChangeValue={handleChangeValue}
            label={"Tài Khoản"}
            name={"taiKhoan"}
            value={valueInput.taiKhoan}
            valueErr={valueErr.taiKhoan}
          />
          <InputCustom
            handleChangeValue={handleChangeValue}
            label={"Mật Khẩu"}
            name={"matKhau"}
            value={valueInput.matKhau}
            valueErr={valueErr.matKhau}
          />
          <InputCustom
            handleChangeValue={handleChangeValue}
            label={"Số điên thoại"}
            name={"soDt"}
            value={valueInput.soDt}
            valueErr={valueErr.soDt}
          />
          <InputCustom
            handleChangeValue={handleChangeValue}
            label={"email"}
            name={"email"}
            value={valueInput.email}
            valueErr={valueErr.email}
          />
          <InputCustom
            handleChangeValue={handleChangeValue}
            label={"Họ tên"}
            name={"hoTen"}
            value={valueInput.hoTen}
            valueErr={valueErr.hoTen}
          />
          {/* Select type customer */}
          <div className="flex items-center mt-7">
            <div className="w-full h-full">
              <select
                value={valueInput.maLoaiNguoiDung}
                className="w-full h-2/3 border shadow-md rounded"
                name="maLoaiNguoiDung"
                onChange={handleChangeValue}
              >
                <option value="">Mã loại khách hàng</option>
                <option value="KhachHang">Khách hàng</option>
                <option value="QuanTri">Quản trị</option>
              </select>
              <div className="h-6 text-red-500">{valueErr.maLoaiNguoiDung}</div>
            </div>
          </div>
        </form>
      </Modal>
    </Fragment>
  );
}
