import React, { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { message, Table } from "antd";
import { userServie } from "../../../service/UserService";
import {
  getListUserAsync,
  setStatusShowEditUser,
  setUserEditAction,
} from "../../../Redux/adminUserSlice";
import AddUser from "./AddUser";
import { createColumn } from "./utils/createTableUser";
import EditUser from "./EditUser";

export default function UserManagement() {
  let dispatch = useDispatch();
  let listUser = useSelector((state) => state.adminUserSlice.listUsers);
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
  };
  const handleReset = (clearFilters) => {
    clearFilters();
  };

  let handleDeleteUser = (TaiKhoan) => {
    let deleteUser = async (TaiKhoan) => {
      try {
        let res = await userServie.deleteUser(TaiKhoan);
        message.success(res.data.content);
        dispatch(getListUserAsync());
      } catch (err) {
        message.error(err.response.data.content);
      }
    };
    deleteUser(TaiKhoan);
  };
  let handleEditUser = (taiKhoanUser) => {
    dispatch(setStatusShowEditUser(true));
    dispatch(setUserEditAction(taiKhoanUser));
  };
  return (
    <main>
      <div className="relative h-20">
        {/* Button add user */}
        <div className="absolute top-0 right-0">
          <AddUser />
        </div>
      </div>
      <Table
        pagination={{ pageSize: 7 }}
        columns={createColumn(
          handleDeleteUser,
          searchInput,
          handleSearch,
          handleReset,
          handleEditUser
        )}
        dataSource={listUser}
      />
      <EditUser />
    </main>
  );
}
