import { message } from "antd";
import {
  getListUserAsync,
  setStatusShowEditUser,
} from "../../../../Redux/adminUserSlice";
import { MA_NHOM } from "../../../../service/urlConfig";
import { userServie } from "../../../../service/UserService";

export const submitCreateFormApi = (
  handleCancel,
  setValue,
  setValueErr,
  valueErr,
  valueInput,
  dispatch
) => {
  let postAddUser = async (dataUser) => {
    try {
      await userServie.addUser(dataUser);
      message.success("Đăng kí thành công");
      handleCancel();
      setValue({
        taiKhoan: "",
        matKhau: "",
        email: "",
        soDt: "",
        maNhom: MA_NHOM,
        maLoaiNguoiDung: "",
        hoTen: "",
      });
      dispatch(getListUserAsync());
    } catch (err) {
      console.log(err);
      if (err.response.data.content == "Email đã tồn tại!") {
        setValueErr({ ...valueErr, email: "Email đã tồn tại!" });
      } else {
        setValueErr({
          ...valueErr,
          taiKhoan: "Tài khoản đã tồn tại!",
        });
      }
      message.error("đăng kí thất bại");
    }
  };
  postAddUser(valueInput);
};

export const submitUpdateFormApi = (valueInput, dispatch) => {
  let putUpdateUser = async (dataUser) => {
    try {
      await userServie.updateUser(dataUser);
      message.success("Cập nhật thành công");
      dispatch(setStatusShowEditUser(false));
      dispatch(getListUserAsync());
    } catch (err) {
      console.log(err);
      message.error("Cập nhật thất bại");
    }
  };
  putUpdateUser(valueInput);
};
