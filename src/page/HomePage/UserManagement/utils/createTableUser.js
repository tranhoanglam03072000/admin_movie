import { Button, Input, Space } from "antd";
import { SearchOutlined } from "@ant-design/icons";

export const createColumn = (
  handleDeleteUser,
  searchInput,
  handleSearch,
  handleReset,
  handleEditUser
) => {
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <button
            className="bg-blue-400 hover:bg-blue-500 hover:text-white text-white rounded"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            style={{
              width: 90,
            }}
          >
            Tìm
          </button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Làm mới
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        className="text-xl"
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) => text,
  });
  return [
    {
      title: "Tài Khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
      render: (text) => <div>{text}</div>,
      ...getColumnSearchProps("taiKhoan"),
      width: "20%",
    },
    {
      title: "Họ tên",
      dataIndex: "hoTen",
      key: "hoTen",
      render: (text) => <div className="h-8">{text}</div>,
      width: "15%",
    },
    {
      title: "email",
      dataIndex: "email",
      key: "email",
      render: (text) => <div>{text}</div>,
      width: "20%",
    },
    {
      title: "Số điện thoại",
      key: "soDT",
      dataIndex: "soDT",
      render: (_, user) => {
        return <div className=" h-9">{user.soDT}</div>;
      },
      width: "15%",
    },
    {
      title: "Loại người dùng",
      key: "maLoaiNguoiDung",
      render: (_, user) => {
        return (
          <span className="rounded border border-purple-500 text-center px-2 py-1">
            {user.maLoaiNguoiDung == "KhachHang" ? "Khách Hàng" : "Quản trị"}
          </span>
        );
      },
      width: "15%",
    },
    {
      title: "Action",
      key: "action",
      render: (_, user) => (
        <div className="space-x-2">
          <button
            onClick={() => {
              handleDeleteUser(user.taiKhoan);
            }}
            className="bg-red-500 text-white px-2 py-1 rounded"
          >
            Xoá
          </button>
          {/* // Edit  */}
          <button
            className="bg-green-500 text-white px-2 py-1 rounded"
            onClick={() => {
              handleEditUser(user.taiKhoan);
            }}
          >
            Sửa
          </button>
        </div>
      ),
      width: "10%",
    },
  ];
};

//  / ===========
