import { message } from "antd";
import { getListUserAsync } from "../../../../Redux/adminUserSlice";
import { MA_NHOM } from "../../../../service/urlConfig";
import { userServie } from "../../../../service/UserService";
const changeValueValidation = (value, name, valueErr, setValueErr) => {
  // Model check regex
  let checkRegex = (regex, message) => {
    if (regex.test(value)) {
      setValueErr({ ...valueErr, [name]: "" });
    } else {
      setValueErr({ ...valueErr, [name]: message });
    }
  };
  //Check Để Trống
  if (value.trim() === "") {
    setValueErr({
      ...valueErr,
      [name]: ` * Vui lòng điền thông tin`,
    });
  } else {
    setValueErr({
      ...valueErr,
      [name]: ``,
    });
  }
  //Check tài khoản
  if (value.length >= 1) {
    switch (name) {
      case "taiKhoan":
        {
          checkRegex(
            /^[a-zA-Z0-9_]*$/,
            "Không được chứa khoảng trắng và ký tự đặc biệt"
          );
        }
        break;
      case "matKhau":
        {
          checkRegex(
            /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
            "Tối thiểu tám ký tự, ít nhất một chữ cái và một số, không chứa kí tự đặc biệt"
          );
        }
        break;
      case "email":
        {
          checkRegex(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            "Email chưa đúng định dạng"
          );
        }
        break;
      case "soDt":
        {
          checkRegex(
            /(84|0[3|5|7|8|9])+([0-9]{8})+$\b/,
            "Vui lòng nhập đúng số điện thoại"
          );
        }
        break;
      case "hoTen": {
        checkRegex(
          /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/,
          "Họ tên phải là chữ"
        );
      }
    }
  }
  // end Check
};

const submitFormValidation = (valueInput, valueErr) => {
  let isValid = true;
  for (let key in valueInput) {
    if (valueInput[key] == "") {
      isValid = false;
    }
  }
  for (let key in valueErr) {
    if (valueErr[key] != "") {
      isValid = false;
    }
  }
  return isValid;
  // Kiểm tra từ sever
  // if (isValid) {
  //   let postAddUser = async (dataUser) => {
  //     try {
  //       await userServie.addUser(dataUser);
  //       message.success("Đăng kí thành công");
  //       handleCancel();
  //       setValue({
  //         taiKhoan: "",
  //         matKhau: "",
  //         email: "",
  //         soDt: "",
  //         maNhom: MA_NHOM,
  //         maLoaiNguoiDung: "",
  //         hoTen: "",
  //       });
  //       dispatch(getListUserAsync());
  //     } catch (err) {
  //       if (err.response.data.content == "Email đã tồn tại!") {
  //         setValueErr({ ...valueErr, email: "Email đã tồn tại!" });
  //       } else {
  //         setValueErr({
  //           ...valueErr,
  //           taiKhoan: "Tài khoản đã tồn tại!",
  //         });
  //       }
  //       message.error("đăng kí thất bại");
  //     }
  //   };
  //   postAddUser(valueInput);
  // } else {
  //   message.error("đăng kí thất bại");
  // }
};
export { changeValueValidation, submitFormValidation };
