import React, { useEffect, useState } from "react";
import {
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { getListUserAsync } from "../../Redux/adminUserSlice";
import { getListMovieAsync } from "../../Redux/adminMovieSlice";
import { Layout, Menu } from "antd";
import UserManagement from "./UserManagement";
import MovieManagement from "./MovieManagement";
import HeaderPage from "../../components/Header/HeaderPage";
const { Sider, Content } = Layout;
export default function HomePage() {
  const [collapsed, setCollapsed] = useState(false);
  const [content, setContent] = useState("user");
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getListUserAsync());
    dispatch(getListMovieAsync());
  }, []);

  let renderContent = () => {
    if (content === "user") {
      return <UserManagement />;
    } else {
      return <MovieManagement />;
    }
  };
  return (
    <div className="flex w-screen h-screen items-center">
      <Layout className="w-full h-full">
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo h-16" />
          <Menu
            theme="dark"
            mode="inline"
            defaultSelectedKeys={["1"]}
            items={[
              {
                key: "1",
                icon: <UserOutlined />,
                label: "Quản lý user",
                onClick: () => {
                  setContent("user");
                },
              },
              {
                key: "2",
                icon: <VideoCameraOutlined />,
                label: "Quản lý phim",
                onClick: () => {
                  setContent("movie");
                },
              },
            ]}
          />
        </Sider>
        <Layout>
          <main className=" flex items-center justify-between py-5 bg-[#011529]">
            <div
              onClick={() => {
                setCollapsed(!collapsed);
              }}
              className="text-white text-xl"
            >
              <MenuFoldOutlined />
            </div>
            <HeaderPage />
          </main>
          <Content
            style={{
              margin: "24px 16px",
              minHeight: 280,
            }}
          >
            {renderContent()}
          </Content>
        </Layout>
      </Layout>
    </div>
  );
}
