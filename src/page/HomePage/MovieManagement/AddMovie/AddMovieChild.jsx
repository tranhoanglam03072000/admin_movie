import React, { useState } from "react";
import { Button, DatePicker, Form, Input, InputNumber, Switch } from "antd";
import { submitCreateFormMovie } from "../apis/apiMovies";
import { MA_NHOM } from "../../../../service/urlConfig";

export default function AddMovieChild() {
  const [valueMovie, setValueMovie] = useState({
    tenPhim: "",
    trailer: "",
    moTa: "",
    ngayKhoiChieu: "",
    dangChieu: false,
    sapChieu: false,
    hot: false,
    danhGia: 0,
    hinhAnh: {},
    maNhom: MA_NHOM,
  });
  const [imgSrc, setImgSrc] = useState("");
  const handleChangeValueMovie = (e) => {
    let { name, value } = e.target;
    setValueMovie({ ...valueMovie, [name]: value });
  };
  const handleChangeDate = (_, value) => {
    setValueMovie({ ...valueMovie, ngayKhoiChieu: value });
  };
  const handleSubmitValueMovie = (e) => {
    e.preventDefault();
    // Tạo đối tượng formData
    let formData = new FormData();
    for (let key in valueMovie) {
      if (key !== "hinhAnh") {
        formData.append(key, valueMovie[key]);
      } else {
        formData.append("File", valueMovie.hinhAnh, valueMovie.hinhAnh.name);
      }
    }
    console.log("formData", formData.get("sapChieu"));
    // Call api
    submitCreateFormMovie(formData);
  };
  const handleChangeSwitch = (name) => {
    return (value) => {
      setValueMovie({ ...valueMovie, [name]: value });
    };
  };
  const handleChangeFile = (e) => {
    // lấy file ra từ e
    let file = e.target.files[0];
    console.log("file", file);
    if (
      file.type === "image/jpeg" ||
      file.type === "image/jpg" ||
      file.type === "image/png" ||
      file.type === "image/gif" ||
      file.type === "image/bmp"
    ) {
      // tạo 1 đối tượng để đọc file
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        let linkBase64 = e.target.result;
        setImgSrc(linkBase64);
      };
      // đem dữ liệu file đưa vào formdata
      setValueMovie({ ...valueMovie, hinhAnh: file });
    }
  };
  return (
    <Form
      labelCol={{
        span: 4,
      }}
      wrapperCol={{
        span: 14,
      }}
      layout="horizontal"
      initialValues={{
        size: "default",
      }}
      size={"default"}
    >
      <Form.Item label="Tên Phim">
        <Input name="tenPhim" onChange={handleChangeValueMovie} />
      </Form.Item>
      <Form.Item label="Trailer">
        <Input name="trailer" onChange={handleChangeValueMovie} />
      </Form.Item>
      <Form.Item label="Mô tả">
        <Input name="moTa" onChange={handleChangeValueMovie} />
      </Form.Item>
      <Form.Item label="Ngày khởi chiếu">
        <DatePicker
          format={"DD/MM/YYYY"}
          onChange={handleChangeDate}
          placeholder="Chọn ngày"
        />
      </Form.Item>
      <Form.Item label="Đang chiếu" valuePropName="checked">
        <Switch
          name="dangChieu"
          className="bg-gray-300"
          onChange={handleChangeSwitch("dangChieu")}
        />
      </Form.Item>
      <Form.Item label="Sắp chiếu" valuePropName="checked">
        <Switch
          name="sapChieu"
          onChange={handleChangeSwitch("sapChieu")}
          className="bg-gray-300"
        />
      </Form.Item>
      <Form.Item label="hot" valuePropName="checked">
        <Switch
          name="hot"
          onChange={handleChangeSwitch("hot")}
          className="bg-gray-300"
        />
      </Form.Item>
      <Form.Item label="Số sao">
        <InputNumber
          onChange={handleChangeSwitch("danhGia")}
          min="1"
          max="10"
        />
      </Form.Item>
      <Form.Item label="Hình ảnh">
        <input
          type="file"
          onChange={handleChangeFile}
          name=""
          id=""
          accept="image/jpeg, image/jpg, image/png,image/gif,image/bmp"
        />
        <br />
        <img src={imgSrc} style={{ width: 100, height: 100 }} alt="" />
      </Form.Item>
      <Form.Item label="Button">
        <Button onClick={handleSubmitValueMovie}>Button</Button>
      </Form.Item>
    </Form>
  );
}
