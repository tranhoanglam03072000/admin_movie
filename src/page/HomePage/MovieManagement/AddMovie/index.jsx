import React, { Fragment, useState } from "react";
import { Button, Modal } from "antd";
import AddMovieChild from "./AddMovieChild";

export default function AddMovie() {
  const [open, setOpen] = useState(false);
  return (
    <Fragment>
      <button
        className="px-2 py-2 bg-purple-500 rounded-lg text-white hover:bg-purple-700 duration-200"
        onClick={() => setOpen(true)}
      >
        Thêm phim
      </button>
      <Modal
        title="Thêm mới phim"
        centered
        open={open}
        onOk={() => setOpen(false)}
        onCancel={() => setOpen(false)}
        width={1000}
        footer={null}
      >
        <AddMovieChild />
      </Modal>
    </Fragment>
  );
}
