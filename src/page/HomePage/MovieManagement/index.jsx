import { message, Table } from "antd";
import React, { useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListMovieAsync } from "../../../Redux/adminMovieSlice";
import { MovieService } from "../../../service/MovieService";
import AddMovie from "./AddMovie";
import { createColumn } from "./utils/createTable";

export default function MovieManagement() {
  const dispatch = useDispatch();
  const listMovie = useSelector((state) => state.adminMovieSlice.listMovies);
  const searchInput = useRef(null);
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
  };
  const handleReset = (clearFilters) => {
    clearFilters();
  };
  const handleDeleteMovie = (maPhim) => {
    let deleteMovie = async (maPhim) => {
      try {
        let res = await MovieService.deleteMovie(maPhim);
        console.log(res);
        message.success("Xoá phim thành công");
        dispatch(getListMovieAsync());
      } catch (err) {
        console.log(err);
        message.error("xoá thất bại");
      }
    };
    deleteMovie(maPhim);
  };
  return (
    <main>
      <div className="relative h-20">
        {/* Button add Movie */}
        <div className="absolute top-0 right-0">
          <AddMovie />
        </div>
      </div>
      <Table
        pagination={{ pageSize: 4 }}
        columns={createColumn(
          handleDeleteMovie,
          searchInput,
          handleSearch,
          handleReset
        )}
        dataSource={listMovie}
      />
    </main>
  );
}
