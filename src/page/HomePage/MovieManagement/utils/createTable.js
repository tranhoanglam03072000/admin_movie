import { Button, Input, Space } from "antd";
import { SearchOutlined } from "@ant-design/icons";

export const createColumn = (
  handleDeleteMovie,
  searchInput,
  handleSearch,
  handleReset
) => {
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div
        style={{
          padding: 8,
        }}
        onKeyDown={(e) => e.stopPropagation()}
      >
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{
            marginBottom: 8,
            display: "block",
          }}
        />
        <Space>
          <button
            className="bg-blue-400 hover:bg-blue-500 hover:text-white text-white rounded"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            style={{
              width: 90,
            }}
          >
            Tìm
          </button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{
              width: 90,
            }}
          >
            Làm mới
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        className="text-xl"
        style={{
          color: filtered ? "#1890ff" : undefined,
        }}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) => text,
  });
  return [
    {
      title: "Mã phim",
      dataIndex: "maPhim",
      key: "maPhim",
      render: (text) => <div>{text}</div>,
      width: "10%",
      sorter: (a, b) => a.maPhim - b.maPhim,
    },
    {
      title: "Tên phim",
      dataIndex: "tenPhim",
      key: "tenPhim",
      render: (text) => <div>{text}</div>,
      width: "20%",
      ...getColumnSearchProps("tenPhim"),
    },

    {
      title: "Hình ảnh",
      key: "hinhAnh",
      dataIndex: "hinhAnh",
      width: "10%",
      render: (_, movie) => {
        return (
          <div className="w-2/3 h-20">
            <img
              src={movie.hinhAnh}
              style={{
                width: "100%",
                height: "100%",
                borderRadius: "10%",
                objectFit: "cover",
              }}
              alt=""
            />
          </div>
        );
      },
    },

    {
      title: "Mô tả",
      dataIndex: "moTa",
      key: "moTa",
      width: "40%",
      render: (text) => <div>{text.substring(0, 100)}...</div>,
    },
    {
      title: "Action",
      key: "action",
      width: "20%",
      render: (_, movie) => (
        <div className="space-x-2">
          <button
            onClick={() => {
              handleDeleteMovie(movie.maPhim);
            }}
            className="bg-red-500 text-white px-2 py-1 rounded"
          >
            Xoá
          </button>
          <button className="bg-green-500 text-white px-2 py-1 rounded">
            Sửa
          </button>
        </div>
      ),
    },
  ];
};

//  / ===========
