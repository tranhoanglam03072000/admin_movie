import React, { useEffect } from "react";
import { Form, Input } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { postLoginAsync, setUserInfo } from "../../Redux/userSlice";
import { useNavigate } from "react-router-dom";
export default function LoginPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  let userInfo = useSelector((state) => state.userSlice.userInfo);
  console.log("userInfo: ", userInfo);
  useEffect(() => {
    if (userInfo) {
      navigate("/");
    }
  }, []);
  const onFinish = (user) => {
    let handleNavige = () => {
      navigate("/");
    };
    dispatch(postLoginAsync({ user, handleNavige }));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-screen h-screen flex items-center justify-center container mx-auto">
      <div className="w-1/2 ">avc</div>
      <div className="w-1/2 bg-red-500">
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Tài Khoản"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật khẩu"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <button
              className="bg-purple-500 text-white hover:bg-purple-600 hover:text-white duration-100 px-3 py-2 rounded"
              type="Submit"
            >
              Submit
            </button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
