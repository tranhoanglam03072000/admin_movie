import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setUserLogOut } from "../Redux/userSlice";

export default function SecureView({ Component }) {
  let userInfo = useSelector((state) => state.userSlice.userInfo);
  let navigate = useNavigate();
  let dispatch = useDispatch();
  useEffect(() => {
    if (!userInfo) {
      alert("Bạn chưa đăng nhập");
      navigate("/login");
    } else if (userInfo.maLoaiNguoiDung !== "QuanTri") {
      alert("Bạn không có quyền truy cập trang admin");
      navigate("/login");
      dispatch(setUserLogOut());
    }
  }, []);
  return <Component />;
}
